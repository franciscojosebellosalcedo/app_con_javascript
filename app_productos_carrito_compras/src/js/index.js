let productos = [
  {
    id: 1,
    nombre: "hamburguesa-1",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/hamburguesa-1.jpeg",
    precio: 3000,
    categoria: {
      nombre: "Hamburguesas",
      id: "Hamburguesas"
    }
  },
  {
    id: 2,
    nombre: "hamburguesa-2",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/hamburguesa-2.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Hamburguesas",
      id: "Hamburguesas"
    }
  },
  {
    id: 3,
    nombre: "hamburguesa-3",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/hamburguesa-3.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Hamburguesas",
      id: "Hamburguesas"
    }
  },
  {
    id: 4,
    nombre: "hamburguesa-4",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/hamburguesa-4.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Hamburguesas",
      id: "Hamburguesas"
    }
  },
  {
    id: 5,
    nombre: "hamburguesa-5",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/hamburguesa-5.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Hamburguesas",
      id: "Hamburguesas"
    }
  },
  {
    id: 6,
    nombre: "hamburguesa-6",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/hamburguesa-6.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Hamburguesas",
      id: "Hamburguesas"
    }
  },
  {
    id: 7,
    nombre: "perro caliente-1",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/perro-caliente-1.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Perro caliente",
      id: "Perro caliente"
    }
  },
  {
    id: 8,
    nombre: "perro caliente-2",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/perro-caliente-2.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Perro caliente",
      id: "Perro caliente"
    }
  },
  {
    id: 9,
    nombre: "perro caliente-3",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/perro-caliente-3.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Perro caliente",
      id: "Perro caliente"
    }
  },
  {
    id: 10,
    nombre: "perro caliente-4",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/perro-caliente-4.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Perro caliente",
      id: "Perro caliente"
    }
  },
  {
    id: 11,
    nombre: "perro caliente-5",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/perro-caliente-5.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Perro caliente",
      id: "Perro caliente"
    }
  },
  {
    id: 12,
    nombre: "perro caliente-6",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/perro-caliente-6.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Perro caliente",
      id: "Perro caliente"
    }
  },
  {
    id: 13,
    nombre: "pizza-1",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/pizza-1.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Pizza",
      id: "Pizza"
    }
  },
  {
    id: 14,
    nombre: "pizza-2",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/pizza-2.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Pizza",
      id: "Pizza"
    }
  },
  {
    id: 15,
    nombre: "pizza-3",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/pizza-3.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Pizza",
      id: "Pizza"
    }
  },
  {
    id: 16,
    nombre: "pizza-4",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/pizza-4.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Pizza",
      id: "Pizza"
    }
  },
  {
    id: 17,
    nombre: "pizza-5",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/pizza-5.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Pizza",
      id: "Pizza"
    }
  },
  {
    id: 18,
    nombre: "pizza-6",
    descripcion: "Lorem Ipsum is simply dummy text of the printing and typesetting industry.",
    imagen: "../img/pizza-6.jpeg",
    precio: 1000,
    categoria: {
      nombre: "Pizza",
      id: "Pizza"
    }
  },


];
let carrito = [];

document.addEventListener("DOMContentLoaded", () => {
  let cantidadProductoCarrito = document.querySelector(".cantidad_productos");
  let cantidadProductosCarritoModal = document.querySelector(".cantidad_productos_carrito");
  const container_modal_carrito = document.querySelector(".container_modal_carrito");
  const ir_carrito = document.querySelector(".ir_carrito");
  const modal_carrito_salir = document.querySelector(".modal_carrito_salir");
  let container_productos_carrito = document.querySelector(".modal_productos_carrito");

  modal_carrito_salir.addEventListener("click", () => {
    container_modal_carrito.classList.remove("mostrar_modal");
  })
  ir_carrito.addEventListener("click", () => {
    if (localStorage.getItem("carrito") === null) {
      carrito.length = 0;
      mostrarProductosCarrito(carrito);
      actualizarCantidadProductosCarrito(carrito);
    }
    container_modal_carrito.classList.add("mostrar_modal");
  });

  actualizarCantidadProductosCarrito(carrito);
  obtenerCarritoLS();

  cargarProductos("Todos", productos);

  const botonesNav = document.querySelectorAll(".btn");
  let arrayBotones = [...botonesNav];
  const container_modal = document.querySelector(".container_modal");
  const modal_salir = document.querySelector(".modal_salir");

  for (let i = 0; i < arrayBotones.length; i++) {
    const boton = arrayBotones[i];
    boton.addEventListener("click", (e) => {
      e.preventDefault();
      botonActivo(boton);

      if (e.currentTarget.id === "Todos") {
        cargarProductos(e.currentTarget.id, productos);
      } else {
        const filtroProductos = productos.filter((producto) => producto.categoria.nombre === e.currentTarget.id);
        cargarProductos(e.currentTarget.id, filtroProductos);
      }
    })
  }
  function actualizarCantidadProductosCarrito(array) {
    if (array.length === 0) {
      cantidadProductoCarrito.textContent = 0;
      cantidadProductosCarritoModal.textContent = 0;
    } else {
      let cantidad = carrito.reduce((acumulador, producto) => acumulador + producto.cantidad, 0);
      cantidadProductoCarrito.textContent = cantidad;
      cantidadProductosCarritoModal.textContent = cantidad;
    }
  }


  function eventoCardProducto() {
    let modal_titulo = document.querySelector(".modal_titulo");
    let modal_imgen = document.querySelector(".modal_imgen");
    let modal_precio = document.querySelector(".modal_precio");
    let modal_descripcion_producto = document.querySelector(".modal_descripcion_producto");
    let btn_agregar_al_carrito = document.querySelector(".btn_agregar_al_carrito");
    const arrayCardproductos = [...document.querySelectorAll(".producto")];

    for (let i = 0; i < arrayCardproductos.length; i++) {
      const cardProducto = arrayCardproductos[i];
      cardProducto.addEventListener("click", (e) => {
        productos.map((producto) => {
          if (producto.id === parseInt(e.currentTarget.id)) {
            btn_agregar_al_carrito.setAttribute("id", producto.id);
            btn_agregar_al_carrito.addEventListener("click", agregarAlCarrito);
            modal_titulo.innerText = producto.nombre;
            modal_imgen.setAttribute("src", producto.imagen);
            modal_precio.innerHTML = producto.precio;
            modal_descripcion_producto.innerText = producto.descripcion;
          }
        });
        container_modal.classList.add("mostrar_modal");
      });
    }
  }


  modal_salir.addEventListener("click", () => {
    container_modal.classList.toggle("mostrar_modal");
  });

  function botonActivo(botonParametro) {
    for (let i = 0; i < arrayBotones.length; i++) {
      const boton = arrayBotones[i];
      boton.classList.remove("activo");
    }

    botonParametro.classList.add("activo");
  }

  function cargarProductos(titulo, lista) {
    const container_productos = document.querySelector(".container_productos");
    const container_titulo = document.querySelector(".container_titulo");
    const fragmento = document.createDocumentFragment();
    container_productos.innerHTML = "";
    container_titulo.innerText = titulo;
    for (let i = 0; i < lista.length; i++) {
      const producto = lista[i];
      const divProducto = document.createElement("DIV");
      divProducto.innerHTML = `
        <div class="producto" id="${producto.id}">
            <img class="producto_imagen" src="${producto.imagen}" alt="imagen">
            <h1 class="producto_titulo">${producto.nombre}</h1>
        </div>
      `
      fragmento.appendChild(divProducto);
    }
    container_productos.appendChild(fragmento);
    eventoCardProducto();
  }


  /*LOGICA CARRITO*/


  function guardarCarritoLS() {
    localStorage.setItem("carrito", JSON.stringify(carrito));
  }

  function obtenerCarritoLS() {
    let data = localStorage.getItem("carrito");
    if (data) {
      carrito = [...JSON.parse(data)];
      actualizarCantidadProductosCarrito(carrito);
    }
  }

  function agregarAlCarrito(e) {
    obtenerCarritoLS();
    if (localStorage.getItem("carrito") === null) {
      carrito.length = 0;
    }
    actualizarCantidadProductosCarrito(carrito)
    let idBoton = parseInt(e.currentTarget.id);
    let producto = productos.find((productoEncontrado) => productoEncontrado.id === idBoton);
    if (carrito.some((productoCarrito) => productoCarrito.id === idBoton)) {
      let indice = carrito.findIndex((pro) => pro.id === idBoton);
      carrito[indice].cantidad++;
      carrito[indice].subTotal = carrito[indice].precio * carrito[indice].cantidad;
      console.log(carrito)
    } else {
      producto.cantidad = 1;
      producto.subTotal = producto.precio;
      carrito.unshift(producto);
    }
    guardarCarritoLS();
    mostrarProductosCarrito(carrito)
    actualizarCantidadProductosCarrito(carrito);
  }
  function eventoBtnEliminarProductoCarrito() {
    const botones = document.querySelectorAll(".btn_eliminar_producto_carrito");
    botones.forEach((boton) => {
      boton.addEventListener("click", (e) => {
        let idBoton = parseInt(e.currentTarget.id);
        eliminarProductoCarrito(idBoton);
      });
    });
  }

  function eliminarProductoCarrito(id) {
    carrito.map((producto, i) => {
      if (producto.id === id) {
        carrito.splice(i, 1);
        mostrarProductosCarrito(carrito);
        actualizarCantidadProductosCarrito(carrito);
        guardarCarritoLS();
      }
    });
  }
  function vaciarCarrito(carrito) {
    carrito.length = 0;
    mostrarProductosCarrito(carrito);
    actualizarCantidadProductosCarrito(carrito);
    guardarCarritoLS();
  }
  const btn_vaciar_carrito = document.querySelector(".btn_vaciar_carrito");
  btn_vaciar_carrito.addEventListener("click", () => {
    vaciarCarrito(carrito);
  })

  function mostrarProductosCarrito(carrito) {
    let total = document.querySelector(".pagar_total");
    container_productos_carrito.innerHTML = "";
    if (carrito.length > 0) {
      let fragmento = document.createDocumentFragment();
      carrito.map((productoCarrito) => {
        let producto = document.createElement("DIV");
        producto.setAttribute("class", "producto_carrito");
        producto.innerHTML = `
        <img class="producto_carrito_img" src="${productoCarrito.imagen}" alt="imagen"  />
        <div class="container_info">
          <h4 class="container_info_titulo">Nombre</h4>
          <p class="container_info_dato">${productoCarrito.nombre}</p>
        </div>
        <div class="container_info">
          <h4 class="container_info_titulo">Precio</h4>
          <p class="container_info_dato">${productoCarrito.precio}</p>
        </div>
        <div class="container_info">
          <h4 class="container_info_titulo">Cantidad</h4>
          <p class="container_info_dato">${productoCarrito.cantidad}</p>
        </div>
        <div class="container_info">
          <h4 class="container_info_titulo">SubTotal</h4>
          <p class="container_info_dato">${productoCarrito.subTotal}</p>
        </div>
        <div class="container_info">
          <button id="${productoCarrito.id}" class="btn_eliminar_producto_carrito"></button>
        </div>
        `;
        fragmento.appendChild(producto);
      });
      console.log(fragmento);
      let total_pagar = carrito.reduce((acumulador, producto) => acumulador + producto.subTotal, 0);
      total.innerHTML = total_pagar;
      container_productos_carrito.appendChild(fragmento);
      eventoBtnEliminarProductoCarrito();
    } else {
      container_productos_carrito.innerHTML = "No hay productos en el carrito";
      total.innerHTML = 0;
    }
  }

  mostrarProductosCarrito(carrito);
  const btn_comprar_productos = document.querySelector(".btn_ralizar_compra");
  btn_comprar_productos.addEventListener("click", () => {
    if (carrito.length === 0) {
      alert("No se ha agregado nada al carrito");
      container_modal_carrito.classList.remove("mostrar_modal")
      return;
    } else {
      const confirmar = window.confirm("¿ Desea comprar todos los productos ?");
      if (confirmar === true) {
        carrito.length = 0;
        localStorage.removeItem("carrito");
        mostrarProductosCarrito(carrito);
        actualizarCantidadProductosCarrito(carrito);
        window.alert("Compra realiazada con exito\n\nGRACIAS POR SU COMPRA");
        container_modal_carrito.classList.remove("mostrar_modal")
        return 0;
      }
    }

  });

});





