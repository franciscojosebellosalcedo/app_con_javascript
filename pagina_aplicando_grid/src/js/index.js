const icon_sol = document.querySelector(".icon_sol");
const icon_luna = document.querySelector(".icon_luna");
let app = document.querySelector(".app");
let container_temas = document.querySelector(".container_temas");
let items = document.querySelectorAll(".nav_item");
let cards = document.querySelectorAll(".card");
let card_imagens = document.querySelectorAll(".card_imagen");
let footer = document.querySelector(".footer");
let header_titulo = document.querySelector(".header_titulo");

icon_luna.addEventListener("click", () => {
    app.classList.add("oscuro");
    container_temas.classList.add("color");
    header_titulo.classList.add("color");

    items.forEach((item) => {
        if (item.classList.contains("activo_claro")) {
            item.classList.remove("activo_claro")
            item.classList.add("activo_oscuro");
        }
        item.classList.remove("color2");
        item.classList.add("color");

    });
    cards.forEach((card) => {
        card.classList.add("card_oscuro");
    });
    card_imagens.forEach((card_imagen) => {
        card_imagen.classList.add("card_imagen_oscuro");
    });
    footer.classList.add("footer_oscuro");
});

icon_sol.addEventListener("click", () => {
    app.classList.remove("oscuro");
    container_temas.classList.remove("color");
    header_titulo.classList.remove("color");

    items.forEach((item) => {
        if (item.classList.contains("activo_oscuro")) {
            item.classList.remove("activo_oscuro")
            item.classList.add("activo_claro");
        }
        item.classList.remove("color");
        item.classList.add("color2");
    });

    cards.forEach((card) => {
        card.classList.remove("card_oscuro");

    });

    card_imagens.forEach((card_imagen) => {
        card_imagen.classList.remove("card_imagen_oscuro");
    });
    footer.classList.remove("footer_oscuro");
});


function activo(boton) {
    if (app.classList.contains("oscuro")) {
        items.forEach((item) => {
            item.classList.remove("activo_oscuro");
        });
        boton.classList.add("activo_oscuro");
    } else {
        items.forEach((item) => {
            item.classList.remove("activo_claro");
        });
        boton.classList.add("activo_claro");
    }
}
items.forEach((item) => {
    item.addEventListener("click", () => {
        activo(item)
    })
});